import './App.css';
import React, { useState } from 'react';
import Advertising from './Advertising/Advertising';
import Games from './Games/Games';
import Hobby from './Hobby/Hobby';
import Politics from './Politics/Politics';
import Sport from './Sport/Sport';

function App() {
  const [news, setNews] = useState(false)
  return (
    <div className="App">

      <button className="btn" onClick={() => setNews('Advertising')}>Advertising News</button>
      <button className="btn" onClick={() => setNews('Games')}>Games News</button>
      <button className="btn" onClick={() => setNews('Hobby')}>Hobby News</button>
      <button className="btn" onClick={() => setNews('Politics')}>Politics News</button>
      <button className="btn" onClick={() => setNews('Sport')}>Sport News</button>

      {news == 'Advertising' ? <Advertising /> : null}
      {news == 'Games' ? <Games /> : null}
      {news == 'Hobby' ? <Hobby /> : null}
      {news == 'Politics' ? <Politics /> : null}
      {news == 'Sport' ? <Sport /> : null}

    </div>
  )
}

export default App;