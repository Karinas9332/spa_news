import React from 'react'

const Hobby = () => {
    return (
        <div>
            <h2>Последние новости:</h2>
            <ul>
                <li>Уроки рисования в Уфе</li>
                <li>Новые теннисные корты вашего города</li>
                <li>Открытие школы вокала</li>
            </ul>
        </div>)
}

export default Hobby;