import React from 'react'

const Politics = () => {
    return (
        <div>
            <h2>Последние новости:</h2>
            <ul>
                <li>Законопроект о территории "Сириус"</li>
                <li>Новый кандидат на пост главы Мордовии</li>
                <li>Совещание Путина по ситуации с короновирусом</li>
            </ul>
        </div>)
}

export default Politics;