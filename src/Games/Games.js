import React from 'react'

const Games = () => {
    return (
        <div>
            <h2>Последние новости:</h2>
            <ul>
                <li>Новая игра Angry birds 2020</li>
                <li>Матч по настольному хоккею в Нижнем Новгороде</li>
                <li>Турнир по шахматам в Омске</li>
            </ul>
        </div>)
}

export default Games;