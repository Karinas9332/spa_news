import React from 'react'

const Advertising = () => {
    return (
        <div>
            <h2>Последние новости:</h2>
            <ul>
                <li>Жилые дома у озера за 3млн.руб</li>
                <li>Уборка дома 200р/час</li>
                <li>Деревянные шкафы недорого</li>
            </ul>
        </div>)
}

export default Advertising;

